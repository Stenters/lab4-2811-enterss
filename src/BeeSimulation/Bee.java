/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab 2: The Flowers and the Bees
 * Author:     Joshua Spleas and Stuart Enters
 * Date:       12/4/2018
 */
package BeeSimulation;

import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;

import java.util.*;

/**
 * Abstract class to represent a Bee randomly placed in a parent container
 * Is responsible for keeping track of its state reacting to external interactions
 */
public abstract class Bee {
    private static final double OFFSET_FACTOR = 1.5;
    protected int fullSpeed = 80;
    protected int slowSpeed = 40;

    private static Map<String, Image> beeImages = new HashMap<>();
    private Pane parent;

    protected StackPane pane;
    protected ImageView imageView;
    protected Point2D position;
    protected Label energyLabel = new Label();

    protected int energy = 16;

    protected static final int SIZE = 40;
    private static final int SLOW_TURNS = 3;

    protected int turnsSlow = 0;

    private double imageHeight;

    protected boolean hasCollided = false;

    protected double xMax;
    protected double xMin;
    protected double yMax;
    protected double yMin;
    private List<Flower> hasCollidedWith;

    /**
     * Main constructor for a Bee object
     *
     * @param parent    parent container to add the bee to
     * @param imagePath path to the image
     */
    public Bee(Pane parent, String imagePath) {
        this.parent = parent;
        pane = new StackPane();
        pane.setPrefWidth(SIZE * OFFSET_FACTOR);
        pane.setPrefHeight(SIZE * OFFSET_FACTOR);


        updateLabel();
        energyLabel.setStyle("-fx-text-fill: #FFFFFF;");

        imageView = new ImageView(getImage(imagePath));
        imageView.preserveRatioProperty().set(true);
        imageView.setFitWidth(SIZE);

        imageHeight = imageView.getImage().getHeight() *
                (imageView.getFitWidth() / imageView.getImage().getWidth());

        double xRange = parent.getWidth() - imageView.getFitWidth();
        xMin = imageView.getFitWidth() * .5;
        xMax = xMin + xRange;
        double yRange = parent.getHeight() - imageHeight;
        yMin = imageHeight * .5;
        yMax = xMin + yRange;

        Random random = new Random();
        position = new Point2D(xMin + random.nextDouble() * xRange,
                yMin + random.nextDouble() * yRange);

        syncPositions();
        imageView.setTranslateZ(2);

        pane.getChildren().add(imageView);
        pane.getChildren().add(energyLabel);
        parent.getChildren().add(pane);

        StackPane.setAlignment(energyLabel, Pos.TOP_RIGHT);

        hasCollidedWith = new ArrayList<>();
    }

    public Point2D getPosition() {
        return position;
    }

    public boolean getHasCollided() {
        return hasCollided;
    }

    /**
     * method to add an amount of energy to the bee
     *
     * @param amount amount to add
     */
    public void addEnergy(int amount) {
        energy += amount;
        updateLabel();
    }

    /**
     * method to remove an amount of energy from the bee, killing the be if it becomes 0
     *
     * @param amount amount to remove
     * @return true if bee should die
     */
    public boolean decrementEnergy(int amount) {
        energy -= amount;
        if (energy <= 0) {
            kill();
        }
        updateLabel();
        return energy <= 0;
    }

    /**
     * Method to be called at the end of a tick, resets some values and decrements energy
     *
     * @return true if bee should die
     */
    public boolean endTick() {
        hasCollided = false;
        --turnsSlow;
        return decrementEnergy(1);
    }

    /**
     * method to kill the bee, removes the image from the parent
     */
    public void kill() {
        parent.getChildren().remove(pane);

        Parent parent = imageView.getParent();
        if (parent instanceof Pane) {
            ((Pane) parent).getChildren().remove(imageView);
            ((Pane) parent).getChildren().remove(energyLabel);
        }
    }

    /**
     * Abstract method to move the bee
     *
     * @param ticks moves a number of ticks, can be fractional
     */
    public abstract void move(double ticks);

    protected boolean move(double ticks, Flower target) {
        if(!hasCollided) {
            double dist = position.distance(target.getPosition());
            if (dist == 0){
                return true;
            }

            position = calculateMovement(ticks, dist, target.position);
            syncPositions();
            return false;
        }
        else return false;
    }

    /**
     * method to handle a collision with another bee,
     *
     * @param otherBee the other bee this one is colliding with
     */
    public void onCollision(BeeContainer otherBee) {
        if (turnsSlow <= 0) {
            stopForTick();
            otherBee.stopForTick();
        }
        turnsSlow = SLOW_TURNS;
        otherBee.setTurnsSlow(SLOW_TURNS);

    }

    /**
     * method to stop the bee for the rest of the tick
     */
    public void stopForTick() {
        hasCollided = true;
    }

    /**
     * static method to get an image based on the path, or construct it if it doesn't exist
     *
     * @param path path to the image
     * @return the image at that path
     */
    private static Image getImage(String path) {
        beeImages.putIfAbsent(path, new Image(path));
        return beeImages.get(path);
    }

    protected void syncPositions() {
        pane.setLayoutX(imageView.getFitWidth() * -.5 + position.getX());
        pane.setLayoutY(imageHeight * -.5 + position.getY());
    }

    private void updateLabel() {
        energyLabel.setText("" + energy);
    }

    public boolean getHasCollidedWith(Flower flower) {
        return hasCollidedWith.stream().anyMatch(f -> f.equals(flower));
    }

    public void setHasCollidedWith(Flower flower) {
        hasCollidedWith.add(flower);
    }


    protected Point2D calculateMovement(double ticks, double dist, Point2D dest) {
        double movement = Math.min(dist, ticks * (turnsSlow <= 0 ? fullSpeed : slowSpeed));
        Point2D normal = dest.subtract(position).normalize();
        return position.add(movement * normal.getX(), movement * normal.getY());
    }

    // --- Added Methods ---

    public void setSpeed(int amount){
        fullSpeed = amount;
        slowSpeed = amount / 2;
    }

    public int getFullSpeed() {
        return fullSpeed;
    }

    public Pane getParent() {
        return parent;
    }

    public double getSize() {
        return SIZE;
    }

    public int getEnergy() {
        return energy;
    }

    public StackPane getStackPane() {
        return pane;
    }
}