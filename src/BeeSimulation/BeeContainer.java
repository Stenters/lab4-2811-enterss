/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab4: Bee Decorator
 * Author:     Stuart Enters
 * Date:       1/8/2019
 */

package BeeSimulation;

import javafx.geometry.Point2D;

/**
 * Class for standardizing access to wrapped bees
 *
 * @author Stuart Enters
 * @version 1.0 created on 1/8/2019 at 8:35
 */
public class BeeContainer {

    private BeeDecorator beeDec;
    private Bee bee;

    BeeContainer(Bee bee, BeeDecorator beeDec) {
        this.bee = bee;
        this.beeDec = beeDec;
    }

    public void move(double ticks) {
        beeDec.move(ticks);
        if (beeDec.bee.getEnergy() <= 0){
            beeDec.kill();
        }
        beeDec.syncPositions();
    }


    public void damage(int value){
        beeDec.damage(value);

    }

    public void decorate(String name) {

        switch (name){
            case "shield":
                updateDecorator(new DecoratorShield(bee));
                break;

            case "homing":
                updateDecorator(new DecoratorHoming(bee));
                break;

            case "deathwish":
                updateDecorator(new DecoratorDeathwish(bee));
                break;

            case "speed":
                updateDecorator(new DecoratorSpeed(bee));
                break;

            default:
                break;
        }

    }

    private void updateDecorator(BeeDecorator tempBee) {
        beeDec.setPrevDec(tempBee);
        tempBee.setBeeDec(beeDec);
        beeDec = tempBee;
    }


    // --- WRAPPED METHODS ---

    public Point2D getPosition(){
        return beeDec.getPosition();
    }

    public boolean endTick(){
        return beeDec.endTick();
    }

    public boolean getHasCollided(){
        return beeDec.getHasCollided();
    }

    public boolean getHasCollidedWith(Flower flower){
        return beeDec.getHasCollidedWith(flower);
    }

    public void addEnergy(int amount){
        beeDec.addEnergy(amount);
    }

    public void onCollision(BeeContainer otherBee){
        beeDec.onCollision(otherBee);
    }

    public void setHasCollidedWith(Flower flower){
        beeDec.setHasCollidedWith(flower);
    }

    public void setTurnsSlow(int slowTurns) {
        beeDec.setTurnsSlow(slowTurns);
    }

    public void stopForTick(){
        beeDec.stopForTick();
    }
}