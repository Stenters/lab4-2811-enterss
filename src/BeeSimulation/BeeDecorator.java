/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab4: Bee Decorator
 * Author:     Stuart Enters
 * Date:       1/8/2019
 */

package BeeSimulation;

import javafx.geometry.Point2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import java.util.List;

/**
 * Class for altering the way a bee functions
 *
 * @author Stuart Enters
 * @version 1.0 created on 1/8/2019 at 8:26
 */
public class BeeDecorator {
    protected Bee bee;
    protected BeeDecorator beeDec;
    protected BeeDecorator prevDec;
    protected ImageView decView;
    protected static List<Flower> flowerList;

    protected static final double SIZE_OFFSET = 10;
    private String imagePath;


    public BeeDecorator(Bee bee){
        setBee(bee);
    }

    public static void setFlowerList(List<Flower> flowers) {
        flowerList = flowers;
    }

    public void setBee(Bee bee) {
        this.bee = bee;
    }

    public void setBeeDec(BeeDecorator beeDec) {
        this.beeDec = beeDec;
    }

    public void setPrevDec(BeeDecorator prevDec) {
        this.prevDec = prevDec;
    }

    protected void setImagePath(String imagePath){
        this.imagePath = imagePath;
    }

    public void move(double amount) {
        bee.move(amount);
        syncPositions();
    }

    public boolean decrimentEnergy(int amount){
        if (bee.decrementEnergy(amount)){
            kill();
            return true;
        }
        return false;
    }

    protected void remove(){
        if (prevDec != null && beeDec != null){
            prevDec.beeDec = beeDec;
            beeDec.prevDec = prevDec;
        } else if (prevDec == null){
            beeDec.prevDec = null;
        } else {
            prevDec.beeDec = null;
        }

        bee.getStackPane().getChildren().remove(decView);
        decView = null;
    }

    public void updateImage(){
        var children = bee.getStackPane().getChildren();
        var offset = getNumDecorations() * SIZE_OFFSET;

        decView = new ImageView(new Image(imagePath));
        decView.setPreserveRatio(true);
        decView.setFitWidth(bee.getSize() + (offset * getNumDecorations()));
        decView.setFitHeight(bee.getSize() + (offset * getNumDecorations()));

        decView.setTranslateZ(getNumDecorations() + 2);
        children.add(decView);
        children.get(children.size() - 1).toBack();
        syncPositions();
    }

    private int getNumDecorations() {
        if (beeDec != null){
            return beeDec.getNumDecorations() + 1;
        }
            else {
                return 1;
        }
    }

    public void syncPositions(){
//        if (beeDec != null) {
//            beeDec.syncPositions();
//        }
//
//        var position = bee.getPosition();
//        if (decView != null){
//            decView.relocate(decView.getFitWidth() * -.5 + position.getX(), decView.getFitHeight() * -.5 + position.getY());
//        }
    }


    public void kill(){
        if (beeDec != null){
            beeDec.kill();
        }

        bee.kill();
        if (decView != null){
            bee.getParent().getChildren().remove(decView);
            decView = null;
        }
    }

    public void damage(int value) {
        if (decrimentEnergy(value)){
            kill();
        }
    }

    public boolean endTick(){
        if (bee.endTick()){
            kill();
            return true;
        }
        return false;

    }

    // --- WRAPPED METHODS ---

    public Point2D getPosition(){
        return bee.getPosition();
    }

    public boolean getHasCollided(){
        return bee.getHasCollided();
    }

    public boolean getHasCollidedWith(Flower flower){
        return bee.getHasCollidedWith(flower);
    }

    public void addEnergy(int amount){
        bee.addEnergy(amount);
    }

    public void onCollision(BeeContainer otherBee){
        bee.onCollision(otherBee);
    }

    public void setHasCollidedWith(Flower flower){
        bee.setHasCollidedWith(flower);
    }

    public void setTurnsSlow(int slowTurns){
        bee.turnsSlow = slowTurns;
    }

    public void stopForTick(){
        bee.stopForTick();
    }
}