/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab 2: The Flowers and the Bees
 * Author:     Joshua Spleas and Stuart Enters
 * Date:       12/4/2018
 */
package BeeSimulation;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Tooltip;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.util.Duration;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Controller class for the application, is responsible for interacting to user input
 * and setting up the simulation when the window loads
 */
public class Controller implements Initializable{
    @FXML
    private BorderPane mainPane;

    @FXML
    private VBox selectorKey;

    @FXML
    private VBox coveringKey;

    @FXML
    private VBox trapKey;

    @FXML
    private VBox energyKey;

    @FXML
    private VBox shieldKey;

    @FXML
    private VBox deathwishKey;

    @FXML
    private VBox homingKey;

    @FXML
    private VBox speedKey;

    private FlowerBed flowerBed;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        flowerBed = new FlowerBed(mainPane);

        setTooltip("A Bee that selects a flower and goes to it", selectorKey);
        setTooltip("A Bee that moves back and forth systematically", coveringKey);
        setTooltip("A flower that damages bees that touch it", trapKey);
        setTooltip("A flower that gives nectar (energy) to the first bee that touches it", energyKey);
        setTooltip("A flower that gives a shield to any bee that touches it", shieldKey);
        setTooltip("A flower that gives a deathwish to any bee that touches it", deathwishKey);
        setTooltip("A flower that gives homing to any bee that touches it", homingKey);
        setTooltip("A flower that gives increased speed to any bee that touches it", speedKey);

//        Tooltip coveringInfo = new Tooltip("A Bee that moves back and forth systematically");
//        setTooltipStyle(coveringInfo);
//        Tooltip.install(coveringKey, coveringInfo);
    }

    private void setTooltip(String message, VBox parent){
        Tooltip newTooltip = new Tooltip(message);
        setTooltipStyle(newTooltip);
        Tooltip.install(parent, newTooltip);
    }

    private void setTooltipStyle(Tooltip tooltip) {
        tooltip.setShowDelay(new Duration(.1));
        tooltip.setFont(new Font(12));
    }

    @FXML
    public void onKeyPressed(KeyEvent event){
        if (event.getCode() == KeyCode.RIGHT) {
            flowerBed.tick();
        }
    }
}
