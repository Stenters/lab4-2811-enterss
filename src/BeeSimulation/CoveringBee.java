/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab 2: The Flowers and the Bees
 * Author:     Joshua Spleas and Stuart Enters
 * Date:       12/4/2018
 */
package BeeSimulation;

import javafx.scene.layout.Pane;

import javafx.geometry.Point2D;

/**
 * Class for defining the actions of a bee that travels the flower bed in a grid
 */
public class CoveringBee extends Bee {
    private Point2D dest;
    private int offset = SIZE;

    /**
     * Constructor for covering bee
     * @param parent the parent to add the bee to
     */
    public CoveringBee(Pane parent){
        super(parent, "file:@../../res/garden_jpgs/bee-3.png");
        dest = new Point2D(Math.random() > .5 ? xMax : xMin, position.getY());
    }

    /**
     * Method for moving a number of ticks across the grid
     * @param ticks moves a number of ticks, can be fractional
     */
    @Override
    public void move(double ticks) {
        if(!hasCollided) {
            if (position.equals(dest)) {
                updateDest();

            } else {
                double dist = position.distance(dest);
                position = super.calculateMovement(ticks, dist, dest);
                syncPositions();
            }
        }
    }

    private void updateDest() {
        double newX = dest.getX() == xMax? xMin : xMax;
        if (position.getY() + offset > yMax) {
            dest = new Point2D(newX, yMax - 5);
            offset = -offset;
        } else if ( position.getY() + offset < 0){
            dest = new Point2D(newX, 5);
            offset = -offset;
        } else {
            dest = new Point2D(newX, position.getY() + offset);
        }
    }
}