/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab4: Bee Decorator
 * Author:     Stuart Enters
 * Date:       1/8/2019
 */

package BeeSimulation;

/**
 * Class for giving a bee homing
 *
 * @author Stuart Enters
 * @version 1.0 created on 1/8/2019 at 8:52
 */
public class DecoratorHoming extends BeeDecorator {
    private Flower target;

    public DecoratorHoming(Bee bee){
        super(bee);
        super.setImagePath("file:@../../res/garden_jpgs/square.png");
        super.updateImage();
    }

    @Override
    public void move(double amount) {
        double minDist = Double.MAX_VALUE;

        for (Flower flower : flowerList) {
            if (minDist > bee.position.distance(flower.getPosition())
                    && flower.getClass() == EnergyFlower.class
                    && !bee.getHasCollidedWith(flower)) {

                target = flower;
            }
        }

        if (bee.move(amount,target)) {
            remove();
        }
    }
}