/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab4: Bee Decorator
 * Author:     Stuart Enters
 * Date:       1/8/2019
 */

package BeeSimulation;

/**
 * Class for giving a bee a one time shield
 *
 * @author Stuart Enters
 * @version 1.0 created on 1/8/2019 at 8:49
 */
public class DecoratorShield extends BeeDecorator {

    public DecoratorShield(Bee bee){
        super(bee);
        super.setImagePath("file:@../../res/garden_jpgs/diamond.png");
        super.updateImage();
    }

    public boolean decrimentEnergy(int amount) {
        remove();
        return false;
    }
}