/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab4: Bee Decorator
 * Author:     Stuart Enters
 * Date:       1/12/2019
 */

package BeeSimulation;

/**
 * Class for giving the bee a speed boost
 *
 * @author Stuart Enters
 * @version 1.0 created on 1/12/2019 at 9:01
 */
public class DecoratorSpeed  extends BeeDecorator{

    private int movesRemaining;

    public DecoratorSpeed(Bee bee){
        super(bee);
        super.setImagePath("file:@../../res/garden_jpgs/circle.jpg");
        super.updateImage();
        bee.setSpeed(bee.getFullSpeed() * 2);
        movesRemaining = 10;
    }

    @Override
    public void move(double amount){
        bee.move(amount);
        movesRemaining--;

        if (movesRemaining <= 0){
            bee.setSpeed(bee.getFullSpeed() / 2);
            remove();
        }
    }

}