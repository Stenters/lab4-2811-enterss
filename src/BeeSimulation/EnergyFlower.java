/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab 2: The Flowers and the Bees
 * Author:     Joshua Spleas and Stuart Enters
 * Date:       12/4/2018
 */
package BeeSimulation;

import javafx.scene.layout.Pane;

/**
 * Class for defining how a energy flower acts
 * Is responsible for giving nectar(energy) to the first bee that collides with it
 */
public class EnergyFlower extends Flower {

    private int energyBoost = 5;

    /**
     * Constructor for energy flower
     * @param pane the pane to append the energy flower to
     */
    public EnergyFlower(Pane pane){
        super(pane,"file:@../../res/garden_jpgs/daisy.png");
    }

    /**
     * Method for energizing the first bee to touch the flower
     * @param bee the bee to energize
     */
    @Override
    public void onCollision(BeeContainer bee){
        if(energyBoost > 0) {
            bee.stopForTick();
            bee.setHasCollidedWith(this);
            bee.addEnergy(energyBoost);
            imageView.setImage(
                    getImage("file:@../../res/garden_jpgs/daisy-dead.png"));
            energyBoost = 0;
        }
    }

}