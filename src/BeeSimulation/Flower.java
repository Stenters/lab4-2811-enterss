/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab 2: The Flowers and the Bees
 * Author:     Joshua Spleas and Stuart Enters
 * Date:       12/4/2018
 */
package BeeSimulation;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.geometry.Point2D;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Abstract class for generalizing the behavior of a flower
 * Is responsible for positioning itself in the parent container
 */
public abstract class Flower {
    private static Map<String, Image> flowerImages = new HashMap<>();
    protected ImageView imageView;
    protected Point2D position;
    private static final int SIZE = 60;

    /**
     * Constructor for creating a flower and adding it to its parent pane
     * @param parent parent container to insert the image into
     * @param imagePath path to the image file
     * @param xCenter what percent width to center the image at, 0 to 1.0, 0.5 is centered
     * @param yCenter what percent height to center the image at, 0 to 1.0, 0.5 is centered
     */
    public Flower(Pane parent, String imagePath, double xCenter, double yCenter){
        imageView = new ImageView(getImage(imagePath));
        imageView.preserveRatioProperty().set(true);
        imageView.setFitWidth(SIZE);

        double imageHeight = imageView.getImage().getHeight() *
                (imageView.getFitWidth() / imageView.getImage().getWidth());

        Random random = new Random();
        double xRange = parent.getWidth() - imageView.getFitWidth();
        double yRange = parent.getHeight() - imageHeight;
        position = new Point2D(imageView.getFitWidth() * xCenter + random.nextDouble() * xRange,
                imageHeight * yCenter + random.nextDouble() * yRange);

        imageView.setLayoutX(imageView.getFitWidth() * -xCenter + position.getX());
        imageView.setLayoutY(imageHeight * -yCenter + position.getY());
        imageView.setTranslateZ(1);

        parent.getChildren().add(imageView);
    }

    /**
     * Constructor for a flower, calls other constructor for a centered image
     * @param parent parent container to insert the image into
     * @param imagePath path to the image
     */
    public Flower(Pane parent, String imagePath){
        this(parent, imagePath, .5, .5);
    }

    /**
     * Helper method for returning the flower's position
     * @return the position of the flower
     */
    public Point2D getPosition(){
        return position;
    }

    /**
     * Overide for the equals method on object
     * @param o the object to check equality of
     * @return if the objects are equal
     */
    @Override
    public boolean equals(Object o){
        boolean equals = o.getClass().getName().equals(this.getClass().getName());
        if (equals){
            Flower otherFlower = (Flower) o;
            equals = otherFlower.getPosition().equals(this.position);
        }

        return equals;
    }

    /**
     * Method for defining what happens on a collision
     * @param bee the bee to collide with
     */
    public abstract void onCollision(BeeContainer bee);

    protected Image getImage(String imagePath) {
        flowerImages.putIfAbsent(imagePath, new Image(imagePath));
        return flowerImages.get(imagePath);
    }
}