/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab 2: The Flowers and the Bees
 * Author:     Joshua Spleas and Stuart Enters
 * Date:       12/4/2018
 */
package BeeSimulation;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.scene.layout.*;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.List;
/**
 * Class that represents a flower bed that simulates interactions between bees and flowers
 */
public class FlowerBed {
    private static final int NUM_SELECTORS = 10;
    private static final int NUM_COVERING = 10;
    private static final int NUM_ENERGY = 30;
    private static final int NUM_TRAP = 10;
    private static final int NUM_SHEILD = 5;
    private static final int NUM_HOMING = 5;
    private static final int NUM_DEATHWISH = 5;
    private static final int NUM_SPEED = 5;


    private static final int STEPS_PER_TICK = 50;
    private static final double TICK_TIME = 1;
    private static final int BEE_COLLISION_DIST = 25;
    private static final int FLOWER_COLLISION_DIST = 15;

    private List<BeeContainer> bees = new ArrayList<>();
    private List<Flower> flowers = new ArrayList<>();

    private Pane simulationArea = new Pane();

    private Timeline tickAnimation;


    /**
     * initializes the container in its parent and adds all of the simulation objects
     * @param parent parent borderpane to insert this into
     */
    public FlowerBed(BorderPane parent){
        simulationArea.setFocusTraversable(true);
        simulationArea.setPrefSize(Double.MAX_VALUE, Double.MAX_VALUE);
        simulationArea.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        simulationArea.setStyle(
                          "-fx-background-image: url(\"file:@../../res/garden_jpgs/dirt.jpg\");\n"
                        + "-fx-background-size: 900;\n"
                        + "-fx-background-position: center center;\n"
        );
        parent.setCenter(simulationArea);
        Platform.runLater(() -> {
            for(int i = 0; i < NUM_ENERGY; ++i){
                flowers.add(new EnergyFlower(simulationArea));
            }
            for(int i = 0; i < NUM_TRAP; ++i){
                flowers.add(new TrapFlower(simulationArea));
            }
            for(int i = 0; i < NUM_SHEILD; ++i){
                flowers.add(new FlowerShield(simulationArea));
            }
            for(int i = 0; i < NUM_HOMING; ++i){
                flowers.add(new FlowerHoming(simulationArea));
            }
            for(int i = 0; i < NUM_DEATHWISH; ++i){
                flowers.add(new FlowerDeathwish(simulationArea));
            }
            for (int i = 0; i < NUM_SPEED; i++){
                flowers.add(new FlowerSpeed(simulationArea));
            }

            SelectorBee.setFlowerList(flowers);
            BeeDecorator.setFlowerList(flowers);

            for(int i = 0; i < NUM_SELECTORS; ++i){
                var bee = new SelectorBee(simulationArea);
                bees.add(new BeeContainer(bee, new BeeDecorator(bee)));
            }
            for(int i = 0; i < NUM_COVERING; ++i){
                var bee = new CoveringBee(simulationArea);
                bees.add(new BeeContainer(bee, new BeeDecorator(bee)));
            }
        });
    }

    /**
     * Method to be called every "tick" of the simulation, runs several steps of movement and
     * collision for the simulation objects
     */
    public void tick(){
        double percent = 1.0 / STEPS_PER_TICK;
        if (tickAnimation == null){
            tickAnimation = new Timeline(
                    new KeyFrame(Duration.seconds(percent * TICK_TIME), (e) -> {
                        for(BeeContainer b : bees){
                            b.move(percent);
                        }
                        checkCollision();
            }));
            tickAnimation.setCycleCount(STEPS_PER_TICK);
            tickAnimation.setOnFinished((e) -> {
                List<BeeContainer> removeList = new ArrayList<>();
                for(BeeContainer b : bees){
                    if (b.endTick()){
                        removeList.add(b);
                    }
                }
                for(BeeContainer b : removeList){
                    bees.remove(b);
                }
            });
        }

        if(tickAnimation.getStatus() != Animation.Status.RUNNING){
            tickAnimation.play();
        }
    }

    private void checkCollision(){
        for(int i = 0; i < bees.size(); ++i){
            if (!bees.get(i).getHasCollided()){
                for (int j = i + 1; j < bees.size(); ++j) {
                    if (!bees.get(j).getHasCollided() &&
                            bees.get(i).getPosition().distance(
                                    bees.get(j).getPosition()) <= BEE_COLLISION_DIST) {
                        bees.get(i).onCollision(bees.get(j));
                    }
                }
                for (Flower f : flowers) {
                    if (!bees.get(i).getHasCollidedWith(f) &&
                            bees.get(i).getPosition().distance(
                            f.getPosition()) <= FLOWER_COLLISION_DIST) {
                        f.onCollision(bees.get(i));
                    }
                }
            }
        }
    }
}