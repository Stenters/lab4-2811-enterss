/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab4: Bee Decorator
 * Author:     Stuart Enters
 * Date:       1/8/2019
 */

package BeeSimulation;

import javafx.scene.layout.Pane;

/**
 * Class for defining a deathwish flower
 *
 * @author Stuart Enters
 * @version 1.0 created on 1/8/2019 at 8:54
 */
public class FlowerDeathwish extends Flower {

    /**
     * Constructor for a deathwish flower
     * @param pane the pane to spawn the flower
     */
    public FlowerDeathwish(Pane pane){
        super(pane,"file:@../../res/garden_jpgs/nightshade.jpg");
    }

    /**
     * Method for defining what happens on a collision
     *
     * @param bee the bee to collide with
     */
    @Override
    public void onCollision(BeeContainer bee) {
        bee.decorate("deathwish");
        bee.setHasCollidedWith(this);
    }
}