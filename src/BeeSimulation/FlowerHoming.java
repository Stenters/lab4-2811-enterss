/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab4: Bee Decorator
 * Author:     Stuart Enters
 * Date:       1/8/2019
 */

package BeeSimulation;

import javafx.scene.layout.Pane;

/**
 * Class for defining a homing flower
 *
 * @author Stuart Enters
 * @version 1.0 created on 1/8/2019 at 8:53
 */
public class FlowerHoming extends Flower {

    public FlowerHoming(Pane pane){
        super(pane,"file:@../../res/garden_jpgs/aster.jpg");
    }

    /**
     * Method for defining what happens on a collision
     *
     * @param bee the bee to collide with
     */
    @Override
    public void onCollision(BeeContainer bee) {
        bee.decorate("homing");
        bee.setHasCollidedWith(this);
    }
}