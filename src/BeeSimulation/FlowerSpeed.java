/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab4: Bee Decorator
 * Author:     Stuart Enters
 * Date:       1/12/2019
 */

package BeeSimulation;

import javafx.scene.layout.Pane;

/**
 * @author Stuart Enters
 * @version 1.0 created on 1/12/2019 at 9:04
 */
public class FlowerSpeed extends Flower {
    public FlowerSpeed(Pane pane) {
        super(pane,"file:@../../res/garden_jpgs/coneflower.jpg");
    }

    /**
     * Method for defining what happens on a collision
     *
     * @param bee the bee to collide with
     */
    @Override
    public void onCollision(BeeContainer bee) {
        bee.decorate("speed");
        bee.setHasCollidedWith(this);
    }
}