/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab 2: The Flowers and the Bees
 * Author:     Joshua Spleas and Stuart Enters
 * Date:       12/4/2018
 */
package BeeSimulation;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Main class for the application, creates the window and initializes FXML
 */
public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader = new FXMLLoader(this.getClass().getResource("flowerBed.fxml"));
        Parent root = loader.load();
        primaryStage.setTitle("Flower Bed");
        primaryStage.setScene(new Scene(root, 900, 800));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
