/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab 2: The Flowers and the Bees
 * Author:     Joshua Spleas and Stuart Enters
 * Date:       12/4/2018
 */
package BeeSimulation;

import javafx.scene.layout.Pane;

import java.util.List;

/**
 * Class for defining the actions of a bee that selects a flower to travel to
 */
public class SelectorBee extends Bee {
    private Flower selectedFlower;
    private static List<Flower> flowerList;

    /**
     * Constructor for selector bee
     * @param parent the pane to draw the bee on
     */
    public SelectorBee(Pane parent){
        super(parent, "file:@../../res/garden_jpgs/bee-1.png");
        updateDest();
    }

    /**
     * Method for moving a selector bee multiple ticks
     * @param ticks moves a number of ticks, can be fractional
     * @author Joshua Spleas
     */
    @Override
    public void move(double ticks) {
        if(!hasCollided) {
            double dist = position.distance(selectedFlower.getPosition());
            if (dist == 0){
                updateDest();
                move(ticks);
            }

            position = super.calculateMovement(ticks, dist, selectedFlower.position);
            syncPositions();
        }
    }
    private void updateDest() {
        double index = Math.floor(Math.random() * flowerList.size());

        while (getHasCollidedWith(flowerList.get((int) index))){
            index = Math.floor(Math.random() * flowerList.size());
        }
        selectedFlower = flowerList.get((int) index);
    }

    /**
     * Method for setting the list of flowers for selector bees to choose from
     * @param flowers the list of flowers to choose from
     */
    public static void setFlowerList(List<Flower> flowers){
        flowerList = flowers;
    }
}