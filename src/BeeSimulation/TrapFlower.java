/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab 2: The Flowers and the Bees
 * Author:     Joshua Spleas and Stuart Enters
 * Date:       12/4/2018
 */
package BeeSimulation;

import javafx.scene.layout.Pane;

/**
 * Class for defining how a trap flower acts
 */
public class TrapFlower extends Flower {

    private final static int ENERGY_DMG = 3;

    /**
     * Constructor for trap flowers
     * @param pane the pane to append the trap flower to
     */
    public TrapFlower(Pane pane){
        //.18 is approximately the center of the flower
        super(pane,"file:@../../res/garden_jpgs/rose.png", .5, .18);
    }

    /**
     * Method for damaging a bee on contact with a trap flower
     * @param bee the bee to damage
     */
    @Override
    public void onCollision(BeeContainer bee) {
        bee.damage(ENERGY_DMG);
        bee.setHasCollidedWith(this);
    }

}